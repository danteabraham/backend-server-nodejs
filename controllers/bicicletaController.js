let Bicicleta = require('../models/bicicleta')


exports.bicicletaList = (req, res) => {
  res.render('bicicletas/index', { bicis: Bicicleta.allBics })
}

exports.bicicletaCreate = (req, res) => {
  res.render('bicicletas/create')
}

exports.bicicletaPost = (req, res) => {
  let newBici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
  newBici.ubicacion = [req.body.lat, req.body.lng]
  Bicicleta.add(newBici)
  res.redirect('/bicicletas')
}

exports.bicicletaDelete = (req, res) => {
  Bicicleta.removeById(req.body.id)
  res.redirect('/bicicletas')
}

exports.bicicletaUpdate = (req, res) => {
  let bici = Bicicleta.findById(req.params.id)
  res.render('bicicletas/update', { bici })
}

exports.bicicletaUpdatePost = (req, res) => {
  let bici = Bicicleta.findById(req.params.id)
  bici.id = req.body.id
  bici.color = req.body.color
  bici.modelo = req.body.modelo
  bici.ubicacion = [req.body.lat, req.body.lng]
  res.redirect('/bicicletas')
}
