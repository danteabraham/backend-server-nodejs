let Bicicleta = function(id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = () => `id: ${this.id} | color: ${this.color}`

Bicicleta.allBics = []
Bicicleta.add = (bic) => Bicicleta.allBics.push(bic)

Bicicleta.findById = (id) => {
  let bici = Bicicleta.allBics.find(x => x.id == id)
  if(bici) {
    return bici
  }
  else {
    throw new Error(`No existe una bicicleta con el id ${id}`)
  }
}

Bicicleta.removeById = (id) => {
  //Bicicleta.findById(id)
  for(let i = 0; i < Bicicleta.allBics.length; i++) {
    if(Bicicleta.allBics[i].id == id) {
      Bicicleta.allBics.splice(i, 1)
      break
    }
  }
}

let a = new Bicicleta(1, 'rojo', 'urbana', [-16.50399, -68.1297908])
let b = new Bicicleta(2, 'blanca', 'urbana', [-16.5038125, -68.1307134])

Bicicleta.add(a)
Bicicleta.add(b)

module.exports = Bicicleta