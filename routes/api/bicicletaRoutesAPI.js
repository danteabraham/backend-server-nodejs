let express = require('express')
let router = express.Router()
let bicicletaController = require('../../controllers/api/bicicletacontrollerAPI')


router.get('/', bicicletaController.BicicletaList)
router.post('/create', bicicletaController.BicicletaCreate)
router.delete('/delete', bicicletaController.BicicletaDelete)

module.exports = router
